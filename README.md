Clone this repository with: `git clone --recurse-submodules <this-repository-url>` so that the submodule: Apache Superset will be cloned as well.

Run setup.sh to start up Apache Superset with Microsoft SQL Server driver installed. The username and password for Apache Superset are both "hackday", and the url is localhost:8088.

#!/usr/bin/env bash

set -ex

# Create an admin user 
fabmanager create-admin --app superset \
	                --username hackday\
	                --password hackday \
			--firstname hack \
			--lastname day \
			--email hack@day.com

# Initialize the database
superset db upgrade

# Load some data to play with
#superset load_examples

# Create default roles and permissions
superset init

# Need to run `npm run build` when enter contains for first time
cd superset/assets && npm run build && cd ../../

# Start superset worker for SQL Lab
superset worker &

# Start the dev web server
flask run -p 8088 --with-threads --reload --debugger --host=0.0.0.0

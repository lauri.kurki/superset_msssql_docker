#!/bin/bash

cp incubator-superset/contrib/docker/{docker-build.sh,docker-entrypoint.sh} incubator-superset
cp {docker-init.sh,Dockerfile,docker-compose.yml} incubator-superset
cp incubator-superset/contrib/docker/superset_config.py incubator-superset/superset/

cd incubator-superset

bash -x docker-build.sh
docker-compose up -d
